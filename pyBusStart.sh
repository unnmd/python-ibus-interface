#!/bin/sh -e
### BEGIN INIT INFO
# Provides:          derekBus.py
# Required-Start:    
# Required-Stop:     
# Default-Start:     
# Default-Stop:      
# Description:       Starts the iBus and Bluetooth script
### END INIT INFO
cd /home/pi/pyBus/
sudo /usr/bin/python derekBus.py &
cd /etc/init.d
