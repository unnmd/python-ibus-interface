import serial, time
import RPi.GPIO as GPIO


class Bluetooth:
	def __init__(self):
		self.serial = serial.Serial(
         	"/dev/ttyAMA0",
			baudrate=115200,
			bytesize=serial.EIGHTBITS,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_ONE
    		)
		GPIO.setmode(GPIO.BOARD)
		GPIO.setup(11, GPIO.OUT)
        #initilize the RN52 for command mode
		GPIO.output(11, True)
		time.sleep(.1) #TODO: convert this to look for the "END\r\n" string, then we don't have to wait arbitrarily
		self.serial.flushInput()
		GPIO.output(11, False)
		self.COMMAND_STRING = "CMD\r\n"
		self.commandPrompt = self.serial.readline()
	
	def pause(self):
		self.serial.write('AP\r')
		print (self.serial.readline()),
	def volUp(self):
		self.serial.write('AV+\r')
		print (self.serial.readline()),
	def volDown(self):
		self.serial.write('AV-\r')
		print (self.serial.readline()),
	def trackNext(self):
		self.serial.write('AT+\r')
		print (self.serial.readline()),
	def trackPrev(self):
		self.serial.write('AT-\r')
		print (self.serial.readline()),
	def reconnect(self):
		self.serial.write('B\r')
		print (self.serial.readline()),
	def callAccept(self):
		self.serial.write('C\r')
		print (self.serial.readline()),
	def callTerminate(self):
		self.serial.write('E\r')
		print (self.serial.readline()),
	
	def ready(self):
		return (self.commandPrompt == self.COMMAND_STRING)

	def __del__(self):
		del self.serial
		GPIO.output(11, GPIO.HIGH) # put the RN52 out of command mode
		GPIO.cleanup()

if __name__ == "__main__":
	blue = Bluetooth()
	if blue.ready():
		#ready for commands
		blue.volUp()

	#close everything at the end
	del blue