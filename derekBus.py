import serial, time, logging, os
import lib.pyBus_interface
import BluetoothDriver


#################################
# Configure Logging for pySel
#################################
LOGFILE = "derekBus.log"
def configureLogging(numeric_level):
  logfile = LOGFILE
  if os.path.exists(logfile):
    compress_old_truncate()
  if not isinstance(numeric_level, int):
    numeric_level=0
  logging.basicConfig(
    filename=logfile, 
    level=numeric_level,
    format='%(asctime)s [%(levelname)s in %(module)s] %(message)s', 
    datefmt='%Y/%m/%dT%I:%M:%S'
  )

disk = 0x00
track = 0x00
iBus = lib.pyBus_interface.ibusFace("/dev/ttyUSB0")
blue = BluetoothDriver.Bluetooth()
configureLogging(1)

cdchangerResponse = [0x18, 0x04, 0xFF, 0x02, 0x00]#, 0xE1]
startplayingResponse = [0x18, 0x0A, 0x68, 0x39, 0x02, 0x09, 0x00, 0x3f, 0x00, 0x03, 0x05]#, 0x76]
writeText = [0x18, 0x06, 0x80, 0x23, 0x42, 0x32, 0x68, 0x69]

cdPollPacket = iBus.packet(src = iBus.nodes["RADIO"], dst = iBus.nodes["CD"], dat = [0x01] )
cdPollPacket.getLength()

CDandTrackStatusNotPlayingResponse = iBus.packet(src = iBus.nodes["CD"], dst = iBus.nodes["RADIO"], dat = [0x39, 0x00, 0x02, 0x00, 0x3F, 0x00, disk, track] )
CDandTrackStatusNotPlayingResponse.getLength()

CDandTrackStatusPlayingResponse = iBus.packet(src = iBus.nodes["CD"], dst = iBus.nodes["RADIO"], dat = [0x39, 0x00, 0x09, 0x00, 0x3F, 0x00, disk, track] )
CDandTrackStatusPlayingResponse.getLength()

trackStartPlaying = iBus.packet(src = iBus.nodes["CD"], dst = iBus.nodes["RADIO"], dat = [0x39, 0x02, 0x09, 0x00, 0x3F, 0x00, disk, track] )
trackStartPlaying.getLength()

trackEndPlaying = iBus.packet(src = iBus.nodes["CD"], dst = iBus.nodes["RADIO"], dat = [0x39, 0x07, 0x09, 0x00, 0x3F, 0x00, disk, track] )
trackEndPlaying.getLength()

RequestCurrentCDandTrackStatus = iBus.packet(src = iBus.nodes["RADIO"], dst = iBus.nodes["CD"], dat = [0x38, 0x00, 0x00] )
RequestCurrentCDandTrackStatus.getLength()

StopPlaying = iBus.packet(src = iBus.nodes["RADIO"], dst = iBus.nodes["CD"], dat = [0x38, 0x01, 0x00] )
StopPlaying.getLength()

StartPlaying = iBus.packet(src = iBus.nodes["RADIO"], dst = iBus.nodes["CD"], dat = [0x38, 0x03, 0x00] )
StartPlaying.getLength()

trackPrev = iBus.packet(src = iBus.nodes["RADIO"], dst = iBus.nodes["CD"], dat = [0x38, 0x05, 0x01] )
trackPrev.getLength()

trackNext = iBus.packet(src = iBus.nodes["RADIO"], dst = iBus.nodes["CD"], dat = [0x38, 0x05, 0x00] )
trackNext.getLength()

changeCD1 = iBus.packet(src = iBus.nodes["RADIO"], dst = iBus.nodes["CD"], dat = [0x38, 0x06, 0x01] )
changeCD1.getLength()

changeCD2 = iBus.packet(src = iBus.nodes["RADIO"], dst = iBus.nodes["CD"], dat = [0x38, 0x06, 0x02] )
changeCD2.getLength()

response = False
transmitTime = 0
changedTrack = False

iBus.writeBusPacket(iBus.nodes["CD"], iBus.nodes["BROADCAST"], [0x02, 0x01])
print "Broadcast 02 01 (I'm here)"

while True:

  try:
    busPacket = iBus.readBusPacket();
  except serial.SerialException:
    iBus.writeBusPacket(iBus.nodes["CD"], iBus.nodes["BROADCAST"], [0x02, 0x01])
    print "Broadcast 02 01 (I'm here)"

  if (len(busPacket.fields["dat"]) != 0):
    if busPacket == cdPollPacket:
      response = True
      print "Responding to poll"
      iBus.writeBusPacket(iBus.nodes["CD"], iBus.nodes["BROADCAST"], [0x02, 0x00])
      logging.debug('Wrote CD changer response')
    
    if busPacket == RequestCurrentCDandTrackStatus:
      logging.debug('Recieved RequestCurrentCDandTrackStatus')
      print "Recieved RequestCurrentCDandTrackStatus"  
      iBus.writeBusPacket(packet = CDandTrackStatusNotPlayingResponse)
      logging.debug('Wrote CDandTrackStatusNotPlayingResponse')
      print "Wrote CDandTrackStatusNotPlayingResponse"
      
    if busPacket == StopPlaying:
      logging.debug('Recieved StopPlaying')
      print "Recieved StopPlaying"
      if blue.ready():
        blue.pause()
      #iBus.writeBusPacket(packet = trackEndPlaying) #seems like I may need these...
      iBus.writeBusPacket(packet = CDandTrackStatusNotPlayingResponse)
      logging.debug('Wrote CDandTrackStatusNotPlayingResponse')
      print "Wrote CDandTrackStatusNotPlayingResponse"

    if busPacket == StartPlaying:
      logging.debug('Recieved StartPlaying')
      print "Recieved StartPlaying"
      trackStartPlaying.fields["dat"][-2:] = disk, track
      iBus.writeBusPacket(packet = trackStartPlaying) #seems like I may need these...
      #iBus.writeBusPacket(packet = CDandTrackStatusPlayingResponse)
      logging.debug('Wrote trackStartPlaying')
      print "Wrote trackStartPlaying"
      # This is all so that we can flash the next or prev symbol on a track change
      # Also, because the radio sends a start playing after the button is depressed
      if (changedTrack == False):  
        if blue.ready():
          blue.pause()
      else:
        changedTrack = False
        print "Just changed track, not going to pause"
        time.sleep(0.6)
        track = 0x00
        trackStartPlaying.fields["dat"][-2:] = disk, track
        iBus.writeBusPacket(packet = trackStartPlaying)
        logging.debug('Wrote trackStartPlaying')
        print "Wrote trackStartPlaying"

    if busPacket == trackPrev:
      changedTrack = True
      logging.debug('Recieved trackPrev')
      print "Recieved trackPrev"
      if blue.ready():
        blue.trackPrev()
        track = 0xcc

    if busPacket == trackNext:
      changedTrack = True
      logging.debug('Recieved trackNext')
      print "Recieved trackNext"
      if blue.ready():
        blue.trackNext()
        track = 0xee

    if busPacket == changeCD1:
      logging.debug('Recieved changeCD1')
      print "Recieved changeCD1"
      if blue.ready():
        blue.pause()

    if busPacket == changeCD2:
      logging.debug('Recieved changeCD2')
      print "Recieved changeCD2"
      if blue.ready():
        blue.reconnect()
  else:
    iBus.writeBusPacket(iBus.nodes["CD"], iBus.nodes["BROADCAST"], [0x02, 0x01])
    print "Broadcast 02 01 (I'm here)"

